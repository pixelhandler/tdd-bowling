/*
  Program for Scoring a Bowing Game

  - The game score ranges between zero and 300.
  - A game has ten frames.
  - A frame has 1-2 rolls.
  - A roll has a pin value or fault.
  - A fault is equivalent to a value of zero.
  - A frame with only one roll is a strike.
  - A frame with two rolls may be a spare.
  - A frame can have a score of zero.
  - A frame may include a value from 1-2 future rolls.
  - The tenth frame may have 2-3 rolls.
  - The current game score for a frame does not include future rolls.
  - A completed game has a score calculated from all frames.
  - The score of a bowler's game is independent from other bowlers' scores.
*/

export default null;
